package com.ribal.ecomplaint.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ribal.ecomplaint.R;
import com.ribal.ecomplaint.activity.AboutActivity;
import com.ribal.ecomplaint.activity.LoginActivity;
import com.ribal.ecomplaint.activity.ProfileActivity;


public class SettingFragment extends Fragment {

    TextView namaTv, emailTv;
    ImageView profileIv;
    Button editBt, aboutBt, logoutBt;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        namaTv = view.findViewById(R.id.nama_tv);
        emailTv = view.findViewById(R.id.email_tv);
        profileIv = view.findViewById(R.id.profileImageView);
        editBt = view.findViewById(R.id.edit_account_btn);
        aboutBt = view.findViewById(R.id.about_btn);
        logoutBt = view.findViewById(R.id.logout_btn);

        editBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProfileActivity.class));
            }
        });
        aboutBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AboutActivity.class));
            }
        });
        logoutBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });
    }
}