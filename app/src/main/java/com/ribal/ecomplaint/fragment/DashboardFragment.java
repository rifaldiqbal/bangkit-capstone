package com.ribal.ecomplaint.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ribal.ecomplaint.R;
import com.ribal.ecomplaint.activity.ReportActivity;
import com.ribal.ecomplaint.activity.ReportStatusActivity;


public class DashboardFragment extends Fragment {

    TextView userTv;
    ImageView profileIv;
    Button reportBt, statusBt;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        userTv = view.findViewById(R.id.user_tv);
        profileIv = view.findViewById(R.id.profileImageView);
        reportBt = view.findViewById(R.id.report_btn);
        statusBt = view.findViewById(R.id.status_btn);


        reportBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ReportActivity.class));
            }
        });
        statusBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ReportStatusActivity.class));
            }
        });

    }
}