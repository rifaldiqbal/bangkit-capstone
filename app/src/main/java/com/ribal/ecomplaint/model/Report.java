package com.ribal.ecomplaint.model;

public class Report {
    private String subject;
    private String loc;
    private String detail;
    private  String url;


    public Report(){

    }

    public Report(String subject, String loc, String detail, String url) {
        this.subject = subject;
        this.loc = loc;
        this.detail = detail;
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
